<html>
<?php

$Nom = htmlspecialchars($_POST['nom']);
$Age = htmlspecialchars($_POST['age']);
$Fonction = htmlspecialchars($_POST['fonction']);
$Sexe = htmlspecialchars($_POST['sexe']);

?>

<style type="text/css">
    .tp {
        text-align: center;
        color: blue;
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        overflow: hidden;
        padding: 15px 10px;
        word-break: normal;
    }

    .tgth {
        text-align: center;
        border-color: black;
        border-style: solid;
        border-width: 1px;
        font-family: Arial, sans-serif;
        font-size: 14px;
        font-weight: normal;
        overflow: hidden;
        padding: 10px 5px;
        word-break: normal;
    }
</style>

  <!-- C'est pas conseillé un div qui n'est pas dans le body -->
  <div class="tp">
<?php
    if (isset($_POST['nom']) && isset($_POST['fonction']) && isset($_POST['age'])) {
      echo ucfirst('Bonjour ' . $Nom . ', Vous avez ' . $Age . ' ans, et votre fonction est ' . $Fonction . '.' . " Vous êtes un $Sexe" . '.' . " Vous pouvez maintenant insérer une image.");
    }
?>
  </div>

  <head>
    <title>Herbegeur d'images</title>
  </head>
  <body>
    <h1 class="tgth"> Envoi de fichier image </h1>
    <form action="/Projet_2_redirect2.php" method="post" enctype="multipart/form-data">
      <table>
        <tr class="tgtd">
          <td>
            <input type="file" name="image" :><br>
            <button type="submit">Envoyer</button>
          </td>
        </tr>
      </table>
    </form>
  </body>
</html>